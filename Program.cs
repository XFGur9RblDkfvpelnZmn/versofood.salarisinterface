﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Algemeen;

namespace VersoFood.SalarisInterface_2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dtStartDate = Proces.dtStringToDate(args[0]);
            DateTime dtEindDate = Proces.dtStringToDate(args[1]);
            string sWerkgeverNr = Proces.SLeesFile(Proces.FiSqlsel("clientnum from msyspar")).Trim();
            string sGewerktCompnr = Proces.SLeesFile(Proces.FiSqlsel("compnr50 from msyspar")).Trim();
            Dictionary<string, List<string>> dictionary = Proces.DlsSqlSel("compnr, catoms[8], catoms[7] from urencategorie");
            Dictionary<string, List<string>> dictionaryPers = Proces.DlsSqlSel("filler1, sdatp, edatp,teamkod from personeel");

            foreach(FileInfo fi in new DirectoryInfo(Algemeen.Parameter.SSbRoot + "\\log").GetFiles("MGS*.LON"))
            {
                string sInhoud = Algemeen.Proces.SLeesFile(fi);
                List<string> lsLijnVerwerkt = new List<string>();
                string[] sLijn = Regex.Split(sInhoud, Environment.NewLine);
                #region SD
                for (int i = 2; i < sLijn.Length; i++)
                {
                    if (sLijn[i].Length >= 87)
                    {
                        string sSalnum = sLijn[i].Substring(23, 5).Trim();
                        if (sSalnum.Length >= 5)
                        {
                            string sDatum = sLijn[i].Substring(62, 6).Trim();
                            string sLijnNieuw = "000302500" + sSalnum + sDatum;
                            string sUC = sLijn[i].Substring(80, 3).Trim();
                            if (dictionary.ContainsKey(sUC))
                            {
                                sLijnNieuw += dictionary[sUC][0];
                            }
                            else if (sUC == sGewerktCompnr)
                            {
                                sLijnNieuw += "1010";
                            }
                            else
                            {
                                if (sUC == "000")
                                {
                                    sLijnNieuw += "0000";
                                }
                            }
                            sLijnNieuw += sLijn[i].Substring(83, 4).Trim();
                            lsLijnVerwerkt.Add(sLijnNieuw);
                        }
                    }
                }
                lsLijnVerwerkt.Sort();
                DateTime dtTempDate = dtStartDate;
                string sTempSalnum = string.Empty;
                int iTempCount = lsLijnVerwerkt.Count;
                for(int i=0; i<iTempCount;i++)
                {
                    string sLijnNieuw = lsLijnVerwerkt[i];
                    string sSalnum = sLijnNieuw.Substring(7, 7);
                    DateTime dtLijn = Proces.dtStringToDate(sLijnNieuw.Substring(14, 6));
                    DateTime dtSdatp = DateTime.MinValue;
                    DateTime dtEdatp = DateTime.MinValue;
                    if (dictionaryPers.ContainsKey(sSalnum))
                    {
                        dtSdatp = Proces.dtStringToDate(dictionaryPers[sSalnum][0]);
                        dtEdatp = Proces.dtStringToDate(dictionaryPers[sSalnum][1]);
                    }
                    sTempSalnum = sSalnum;
                    while (dtLijn > dtTempDate && dtTempDate >= dtSdatp && dtTempDate <= dtEdatp)
                    {
                        string sLijnInsert = sLijnNieuw.Substring(0,14)+dtTempDate.ToString("yyMMdd")+"00000000";
                        lsLijnVerwerkt.Insert(i, sLijnInsert);
                        
                        iTempCount = lsLijnVerwerkt.Count;
                        dtTempDate = dtTempDate.AddDays(1);
                        i++;
                    }
                    dtTempDate = dtLijn.AddDays(1);
                    
                    //als volgende record niet van zelfde salnum
                    if (i + 1 >= iTempCount || (i + 1 < iTempCount && sSalnum != lsLijnVerwerkt[i + 1].Substring(7, 7)))
                    {
                        
                        //check of uitgevuld tot einddatum
                        while (dtTempDate <= dtEindDate && dtTempDate >= dtSdatp && dtTempDate <= dtEdatp)
                        {
                            string sLijnInsert = sLijnNieuw.Substring(0, 14) + dtTempDate.ToString("yyMMdd") + "00000000";
                            lsLijnVerwerkt.Insert(i+1, sLijnInsert);
                            iTempCount = lsLijnVerwerkt.Count;
                            dtTempDate = dtTempDate.AddDays(1);
                            i++;
                        }
                        dtTempDate = dtStartDate;
                    }
                    
                }
                StreamWriter sw = new StreamWriter(Parameter.SSbRoot+"\\log\\expblox1.txt");
                foreach (string s in lsLijnVerwerkt)
                {
                    sw.WriteLine(s);
                }
                sw.Close();
                #endregion
                #region securex
                List<string> lsNieuwSec = new List<string>();
                
                for (int i = 0; i < sLijn.Length; i++)
                {
                    if (sLijn[i].Length >= 87 && sLijn[i].Substring(24, 1) != " " && !sLijn[i].Substring(0, 23).Contains(" "))
                    {
                        string sSalnum = sLijn[i].Substring(23, 5).Trim();
                        int tmpSalnum = 0;
                        Int32.TryParse(sSalnum,out tmpSalnum);
                        if (sSalnum.Length >= 5 && dictionaryPers.ContainsKey(sSalnum) || dictionaryPers.ContainsKey(tmpSalnum.ToString()))
                        {
                            if(!dictionaryPers.ContainsKey(sSalnum))
                            {
                                sSalnum = tmpSalnum.ToString();
                            }
                            string teamkod = dictionaryPers[sSalnum][2];
                            if (teamkod.Contains("/"))
                            {
                                teamkod = teamkod.Split('/')[0];
                            }
                            else
                            {
                                 teamkod = "FOUT99";
                            }
                            string sLoonCode = string.Empty;
                            string sUC = sLijn[i].Substring(80, 3).Trim();
                            if (dictionary.ContainsKey(sUC))
                            {
                                sLoonCode += sUC;
                            }
                            else if (sUC == sGewerktCompnr)
                            {
                                sLoonCode += "DA";
                            }
                            while (sLoonCode.Length < 3 && sLoonCode.Trim() != string.Empty)
                            {
                                sLoonCode += " ";
                            }
                            lsNieuwSec.Add("41462" + sLijn[i].Substring(5, 18) + sLijn[i].Substring(23, 57) + sLoonCode + sLijn[i].Substring(83));
                        }
                    }
                    else if (sLijn[i].Length >= 87 && sLijn[i].Substring(24, 1) == " " && !sLijn[i].Substring(0,23).Contains(" "))
                    {
                        //string sSalnum = sLijn[i].Substring(23, 5).Trim();
                        //if (sSalnum.Length >= 5 && dictionaryPers.ContainsKey(sSalnum))
                        //{
                        //    string teamkod = dictionaryPers[sSalnum][2];
                        //    if (teamkod.Contains("/"))
                        //    {
                        //        teamkod = teamkod.Split('/')[0];
                        //    }
                        //    else
                        //    {
                                
                        //        teamkod = "FOUT99";
                        //    }

                            lsNieuwSec.Add("41462" + sLijn[i].Substring(5, 18) + sLijn[i].Substring(23));
                        //}
                    }
                    else if(sLijn[i].Length>23 && !sLijn[i].Substring(19,4).Contains(" "))
                    {
                        lsNieuwSec.Add(sLijn[i].Replace("03025", "41462"));
                    }
                    //i++;
                }
                #endregion
                string sNaam = fi.Name;
                fi.Delete();
                sw = new StreamWriter(Parameter.SSbRoot + "\\log\\"+sNaam);
                List<string> lsNieuwSecOrdered = new List<string>();
                lsNieuwSecOrdered.Add(lsNieuwSec[0]);
                lsNieuwSec.RemoveAt(0);
                lsNieuwSec.Sort(delegate(string x,string y) 
                {
                    if (x.Contains("C1") && !y.Contains("C1") && x.Substring(10,5)==y.Substring(10,5))
                        return -1;
                    else
                        return string.Compare(x.Substring(10, 10), y.Substring(10, 10));
                });
                lsNieuwSecOrdered.AddRange(lsNieuwSec);
                foreach (string s in lsNieuwSecOrdered)
                {
                    sw.WriteLine(s);
                }
                sw.Close();
            }
            Proces.VerwijderWrkFiles();
        }
    }
}
